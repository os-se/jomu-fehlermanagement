package de.hfu;

import java.util.Scanner;

/**
 * Hauptklasse des Praktikumprojekts
 */
public class App {

    public static void main( String[] args ) {
        System.out.println( "OSSE Praktikum Projekt" );
        Scanner scanner = new Scanner(System.in);
        System.out.println("Uppercase string: " + scanner.nextLine().toUpperCase());
        scanner.close();
    }
}