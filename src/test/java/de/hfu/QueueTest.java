package de.hfu;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import de.hfu.unit.Queue;

public class QueueTest {
    
    @Test
    void testEnqueueAndDequeueGueltigeWerte() {
        Queue queue = new Queue(3);

        queue.enqueue(1);
        queue.enqueue(2);
        queue.enqueue(3);

        assertEquals(1, queue.dequeue());
        assertEquals(2, queue.dequeue());
        assertEquals(3, queue.dequeue());

        queue.enqueue(4);

        assertEquals(4, queue.dequeue());
    }

    @Test
    void testEnqueueAndDequeueUngueltigeWerte() {
        Queue queue = new Queue(3);

        queue.enqueue(1);
        queue.enqueue(2);
        queue.enqueue(3);

        assertNotEquals(3, queue.dequeue());
        assertEquals(2, queue.dequeue());
        assertNotEquals(1, queue.dequeue());
    }

    @ParameterizedTest
    @ValueSource(ints = {-1, 0})
    void testEnqueueAndDequeueExceptions(int wert) {
        Queue queue = new Queue(3);

        assertThrows(IllegalArgumentException.class, () -> {new Queue(wert);}, "IllegalArgumentException was expected");
        assertThrows(IllegalStateException.class, () -> {queue.dequeue();}, "IllegalStateException was expected");
    }
}