package de.hfu;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import de.hfu.unit.Util;

import static org.junit.jupiter.api.Assertions.*;

public class UtilTest {
    
    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5, 6})
    void testIstErstesHalbjahrGueltigeMonate(int monat) {
        assertTrue(Util.istErstesHalbjahr(monat));
    }

    @ParameterizedTest
    @ValueSource(ints = {7, 8, 9, 10, 11, 12})
    void testIstErstesHalbjahrUngueltigeMonate(int monat) {
        assertFalse(Util.istErstesHalbjahr(monat));
    }

    @ParameterizedTest
    @ValueSource(ints = {-1, 0, 13, 14})
    void testIstErstesHalbjahrExceptions(int monat) {
        assertThrows(IllegalArgumentException.class, () -> {Util.istErstesHalbjahr(monat);}, "IllegalArgumentException was expected");
    }

}